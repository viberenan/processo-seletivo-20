package com.hepta.funcionarios.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDao;

@Path("/setor")
public class SetorService {
	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private SetorDao dao;

	public SetorService() {
		dao = new SetorDao();
	}

	/**
	 * LISTA TODOS OS SETORES
	 * 
	 * @return 200(OK) - Conseguiu Listar
	 */
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response SetorRead() {
		List<Setor> setor = new ArrayList<>();
		try {
			setor = dao.getAll();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Setor").build();
		}

		GenericEntity<List<Setor>> entity = new GenericEntity<List<Setor>>(setor) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}
	
	/**
	 * CADASTRAR O SETOR
	 * @param setor : Setor a ser cadastrado
	 * @return 201(CREATED) - Conseguiu Cadastrar
	 */
	@Path("/")
	@POST
	public Response setorCreated(Setor setor) {
		dao.save(setor);
		return Response.status(Status.CREATED).build();
	}

}
