package com.hepta.funcionarios.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.persistence.FuncionarioDAO;

@Path("/funcionarios")
public class FuncionarioService {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private FuncionarioDAO dao;

	public FuncionarioService() {
		dao = new FuncionarioDAO();
	}

	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * ADICIONAR NOVO FUNCIONÁRIO
	 * 
	 * @param setorId:     ID do Setor vinculado ao funcionário
	 * @param Funcionario: Funcionário adicionado
	 * @return Response 201 (CREATED) - Conseguiu Cadastrar
	 */
	@Path("/{setorId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response FuncionarioCreate(@PathParam("setorId") long setorId, Funcionario Funcionario) {
		dao.save(setorId, Funcionario);
		return Response.status(Status.CREATED).build();
	}

	/**
	 * Lista todos os Funcionarios
	 * 
	 * @return response 200 (OK) - Conseguiu listar
	 */
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response FuncionarioRead() {
		List<Funcionario> Funcionarios = new ArrayList<>();
		try {
			Funcionarios = dao.getAll();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Funcionarios").build();
		}

		GenericEntity<List<Funcionario>> entity = new GenericEntity<List<Funcionario>>(Funcionarios) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}

	/**
	 * ATUALIZA FUNCIONARIO
	 * 
	 * @param setorId:     Id do Setor
	 * @param produtoId:   Id do funcionário a atualizar
	 * @param funcionario: Funcionario Atualizado
	 * @return 204 (No content) - Funcionário atualizado com sucesso
	 */
	@Path("/{funcionarioId}/{setorId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PUT
	public Response FuncionarioUpdate(@PathParam("setorId") long setorId, @PathParam("funcionarioId") Integer funcionarioId,
			Funcionario funcionario) {
		funcionario.setId(funcionarioId);
		dao.update(setorId, funcionario);
		return Response.noContent().build();
	}

	/**
	 * REMOVE UM FUNCIONÁRIO
	 * 
	 * @param funcionarioId: Id do funcionário a ser removido
	 * @return 204 (No content) - Funcionário removido com sucesso
	 */
	@Path("/{funcionarioId}")
	@Produces(MediaType.APPLICATION_JSON)
	@DELETE
	public Response FuncionarioDelete(@PathParam("funcionarioId") int funcionarioId) {
		dao.delete(funcionarioId);
		return Response.noContent().build();
	}

	/**
	 * Métodos simples apenas para testar o REST
	 * 
	 * @return
	 */
	@Path("/teste")
	@Produces(MediaType.TEXT_PLAIN)
	@GET
	public String TesteJersey() {
		return "Funcionando.";
	}

}
