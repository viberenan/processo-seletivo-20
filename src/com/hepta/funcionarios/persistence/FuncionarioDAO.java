package com.hepta.funcionarios.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.exceptions.DAOException;
import com.hepta.funcionarios.exceptions.ErrorCode;

public class FuncionarioDAO {

	public void save(long setorId, Funcionario funcionario) {
		EntityManager em = HibernateUtil.getEntityManager();
		Setor setor = null;
		try {
			em.getTransaction().begin();
			setor = em.find(Setor.class, setorId);
			funcionario.setSetor(setor);
			em.persist(funcionario);
			em.getTransaction().commit();
		} catch (NullPointerException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Setor informado não existe: " + ex.getMessage(), ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			throw new DAOException("Error ao Salvar Funcionário no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
	}

	public Funcionario update(long setorId, Funcionario funcionario) {
		EntityManager em = HibernateUtil.getEntityManager();
		Funcionario funcionarioAtualizado = null;

		if (funcionario.getId() <= 0) {
			throw new DAOException("Id do Funcionário não pode ser 0", ErrorCode.BAD_REQUEST.getCode());
		}
		try {
			em.getTransaction().begin();
			funcionarioAtualizado = em.find(Funcionario.class, funcionario.getId());
			funcionarioAtualizado.setEmail(funcionario.getEmail());
			funcionarioAtualizado.setNome(funcionario.getNome());
			funcionarioAtualizado.setSalario(funcionario.getSalario());
			funcionarioAtualizado.setIdade(funcionario.getIdade());
			if (funcionarioAtualizado.getSetor().getId() != setorId) {
				Setor setor = em.find(Setor.class, setorId);
				funcionarioAtualizado.setSetor(setor);
			}

			em.getTransaction().commit();
		} catch (NullPointerException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Setor ou Funcionário não existe: " + ex.getMessage(),
					ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao atualizar produto no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
		return funcionarioAtualizado;
	}

	public void delete(int funcionarioId) {
		EntityManager em = HibernateUtil.getEntityManager();
		Funcionario funcionario = null;
		try {
			em.getTransaction().begin();
			funcionario = em.find(Funcionario.class, funcionarioId);
			em.remove(funcionario);
			em.getTransaction().commit();
		} catch (IllegalArgumentException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Funcionário informado para remoção não existe: " + ex.getMessage(),
					ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao remover Funcionário do banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

	}

	public Funcionario find(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Funcionario Funcionario = null;
		try {
			Funcionario = em.find(Funcionario.class, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return Funcionario;
	}
	
	

	@SuppressWarnings("unchecked")
	public List<Funcionario> getAll() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<Funcionario> Funcionarios = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Funcionario");
			Funcionarios = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return Funcionarios;
	}

}
