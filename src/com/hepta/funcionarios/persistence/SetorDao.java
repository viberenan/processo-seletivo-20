package com.hepta.funcionarios.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.exceptions.DAOException;
import com.hepta.funcionarios.exceptions.ErrorCode;

public class SetorDao {
	public Setor find(Long id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Setor Setor = null;
		try {
			Setor = em.find(Setor.class, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return Setor;
	}

	public void save(Setor setor) {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(setor);
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			throw new DAOException("Error ao Salvar Setor no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Setor> getAll() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<Setor> setor = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Setor");
			setor = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return setor;
	}

}
