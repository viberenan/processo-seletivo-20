var inicio = new Vue({
	el:"#inicio",
    data: {
        lista: [],
		listaSetor: [],
		funcionario: {
			id: '',
			nome:'',
			salario: '',
			idade: '',
			email: '' ,
		},
		selectedIndex: 0,
		setor: '' 
    },
    created: function(){
        let vm =  this;
        vm.listarFuncionarios();
        vm.listarSetor();
    },
    methods:{
	// Busca os itens para a lista da primeira página
        listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rest/funcionarios")
			.then(response => {vm.lista = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		
		// Busca os setores para preencher no select
		listarSetor: function() {
			const vm = this;
			axios.get("/funcionarios/rest/setor")
			.then(response => {vm.listaSetor = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		
		// Pega o index do select e salva a id do setor
		switchView: function(event, selectedIndex) {
			this.selectedIndex = selectedIndex; 
			this.setor = this.listaSetor[this.selectedIndex].id;
		},
		
		// salvar Funcionário
		salvar(){
			if(!this.funcionario.id){
				axios.post("/funcionarios/rest/funcionarios/" + this.setor, this.funcionario)
				.then(response => alert('Salvo com Sucesso'))
				.catch(error => alert(error))
			}
			axios.put("/funcionarios/rest/funcionarios/"+ this.funcionario.id + "/" + this.setor, this.funcionario)
			.then(response=> this.listarFuncionarios())
			.catch(error => alert(error))
		},
		
		// Deletar Funcionário
		remover(id){
			if(confirm('Deseja Excluir ?')){
				axios.delete("/funcionarios/rest/funcionarios/" + id)
				.then(response =>{  
					this.listarFuncionarios()
				})
				.catch(error => alert(error))
			}		
		},
		
		// Atualiziar Funcionário
		atualizar(setorId, funcionario){
			this.funcionario = funcionario;
			this.setor = setorId;
		}
    }
});